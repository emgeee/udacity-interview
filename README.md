Udacity Interview - Messaging system
===

Matt Green, September 2013

#### Goal
The task was to design and implement a basic user messaging system complete with
user to user and user to group messaging.

Server: [54.235.171.54](http://54.235.171.54)

#### Objectives
- [x] User to user messaging based on unique user ID
- [x] Messages displayed in Markdown
- [ ] Recipient auto-complete*
- [x] View user inbox
- [x] Read messages
- [x] Mark messages as read when viewed
- [x] Delete messages
- [x] Broadcast messages to all users
- [x] Sending messages to groups of users
- [x] Groups stored as list in user models

\* Auto-complete for both users and groups can be implemented easily using
existing Nodejs libraries on the backend combined with JQuery-UI on the frontend.
MongoDB also has the ability to quickly conduct Regex based partial searches, 
which can easily be used to implement auto-complete.

#### Stack
The system uses a basic Node.js server with a MongoDB database running together on an AWS micro instance.

#### Running locally
Ensure you have Nodejs and MongoDB installed.
Tested with Node v0.10.15

```
cd <ROOT_DIR>
npm install
node app.js
```
Navigate to [http://localhost:3000](http://localhost:3000)

Unit tests can be run using mocha:
```
npm install -g mocha
mocha
```


#### Architecture
The messaging system is very basic and consists of two simple database models: users
and messages. Messages are stored as sub-records in a list that is part of a user record. Basic
routes exist for creating and deleting users as well as viewing a particular user's
inbox. 

Messages are sent by specifying the recipient's MongoDB ID along with the
message, which will cause the recipient's user record to be queried from the database
and the message appended to their record. 

Group functionality is implemented by storing a list of strings as part of the user record that signify group membership. When a message is sent to a group, all user records that contain the group name in their groups array will have the message appended to their messages array. The sender of the message is excluded from receiving this message. 

Broadcast to all users is implemented by sending a message to a special group named
'all'. Doing so will query the database for all user records (minus the sender's) 
and append the message accordingly.

#### Design issues
The biggest issue with this architecture is that it will not scale well. MongoDB
is at its best when searching documents or modifying them in place. Because
the messages are stored as part of the user record, sending and deleting
messages will change the size of a user record, which is very expensive for the
database to handle.

One way to address this issue is by implementing a centralized AMQP server such
as RabbitMQ. Each user can have their own queue on this server that will receive
messages. When a user wants to check his or her messages, the Nodejs server 
handling the request can query this queue and receive the messages. The benefit
of using a queue server is it allows database writes and updates to be tied to
when a user checks for new messages as opposed to when they are sent. This allows
a user to message many thousands (or millions) of users without the database having
to update those user records immediately.

I did try to implement this solution but after several hours of wrestling
with issues in the node-amqp library I opted to revert back to the inefficient,
yet functional method described above.


#### Finished Product
The final product is a web interface that allows for all the functionality 
listed above to be utilized. Although I consider myself much more of a back
end developer, I thought it important to build out enough of the front end
to demo all the features. Needless to say, it is extremely rudimentary.







