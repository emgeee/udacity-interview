
/**
 * Module dependencies.
 */

var express = require('express');
var user = require('./routes/user');
var message = require('./routes/message');
var http = require('http');
var path = require('path');
var db = require('./lib/database.js');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// As a note: 
// Delete routes should use DELETE method to 
// be RESTful. Using POST to minimize frontend JS for 
// ease of prototyping
app.get('/', user.list);
app.get('/user/:id', user.inbox);
app.get('/user/:id/message/:msg_id', message.view);
app.post('/user', user.new);
app.post('/user/:id/delete', user.delete);
app.post('/user/:id/message', message.new);
app.post('/user/:id/message/:msg_id/delete', message.delete);
app.post('/user/:id/group', user.add_to_group);
app.post('/user/:id/group/delete', user.remove_from_group);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

module.exports = app;