var mongoose = require('mongoose');

var db = mongoose.createConnection('localhost', 'udacity', '27017');

db.once('open', function() {
	console.log("connected to mongo!");
});

exports.db = db;

var MessageSchema = new mongoose.Schema({
	body: String,
	from: String,
	from_id: mongoose.Schema.ObjectId,
	unread: Boolean
});

var UserSchema = new mongoose.Schema({
	name: String,
	groups: [String],

	// Nest messages in UserSchema: because message is part of 
	// User, message duplication is possible in the DB however
	// this method makes it easier to delete messages
	// Tradeoff: Simpler coder for less database efficiency
	messages: [MessageSchema]
});

exports.Message = db.model('Message', MessageSchema);
exports.User = db.model('User', UserSchema);

