var db = require('../lib/database');
var mongoose = require('mongoose');
var marked = require('marked');

marked.setOptions({
  gfm: true,
  highlight: function (code, lang, callback) {
    pygmentize({ lang: lang, format: 'html' }, code, function (err, result) {
      if (err) return callback(err);
      callback(null, result.toString());
    });
  },
  tables: true,
  breaks: false,
  pedantic: false,
  sanitize: true,
  smartLists: true,
  smartypants: false,
  langPrefix: 'lang-'
});

/**
 * GET /user/:id/message/:msg_id
 * view a message
 */
exports.view = function(req, res) {
	var id = req.params.id;
	db.User.findById(id, function(error, user){
		if(error || !user) {
			console.error(error);
			res.send("no message :(");
		}
		
		var message = user.messages.id(req.params.msg_id);
		message.unread = false;

		user.save(function(error){
			if(error){
				console.error(error);
			}

			message = message.toObject();

			message.body = marked(message.body);
			
			return res.render('message', {
				message: message,
				recipient_id: id
			});
			
		});
	});
}


/**
 * POST /user/:id/message
 * Create a new message
 */
exports.new = function(req, res) {

	var body = req.body.body;
	var from_id = req.params.id;
	var from = req.body.name;
	var to = req.body.to;
	var group = req.body.group;

	if(to && to != ''){
		db.User.findById(to, function(error, user){
			if(error || !user) {
				console.error(error);
				res.send("no message :(");
			}

			user.messages.push({
				body: body,
				from: from,
				from_id: from_id,
				unread: true
			});

			user.save(function(error){
				return res.redirect('/user/' + from_id);
			});
		});
	} else if (group && group != ''){

		var query = {_id: { $ne: mongoose.Types.ObjectId(from_id) }};

		if(group !== 'all'){
			query.groups = group;			
		}

		db.User.update(query,
			{ $push: { messages: {
				body: body,
				from: from,
				from_id: from_id,
				unread: true
			}}}, {multi: true},
			function(error){
				if(error){
					console.error(error);
				}

				return res.redirect('/user/' + from_id);
			})


	} else{
		res.redirect('/user/' + from_id);
	}
}

/**
 * POST /user/:id/message/:msg_id/delete
 * Delete a message
 */
exports.delete = function(req, res) {
	db.User.findById(req.params.id, function(error, user){
		if(error || !user) {
			console.error(error);
			res.redirect('/user/'+req.params.id);
		}

		user.messages.id(req.params.msg_id).remove();

		user.save(function(error){
			if(error) {
				console.error(error);
			}
			res.redirect('/user/'+req.params.id);
		});
	});
}