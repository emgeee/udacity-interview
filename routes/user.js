var db = require('../lib/database');

/*
 * GET /
 * users listing.
 */
exports.list = function(req, res){

	db.User.find({}, function(error, users) {
		if(error||!users){
			res.send("Error :(");
		}
		console.log(users);
		res.render('user_list', {users: users});

	});
};


/**
 * GET /user/:id
 * View user's inbox
 */
exports.inbox = function(req, res){
	
	db.User.findById(req.params.id, function(error, user){
		if(error || !user) {
			console.error(error);
			return res.send("no user :(");
		}

		console.log(user.messages)
		return res.render('inbox', {user: user});

	});
};


/**
 * POST /user
 * Add a new user
 */
exports.new = function(req, res){
	if(!req.body.name){res.redirect('/');}
	else{
		var user = new db.User({name: req.body.name});
		user.save(function(error, user){
			if(error || !user) {
				console.error(error);
			}

			res.redirect('/');
		});
	}

}


/**
 * POST /user/:id/delete
 * Remove a new user
 */
exports.delete = function(req, res){

	db.User.findByIdAndRemove(req.params.id, function(error, user){
		if(error){
			console.error(error);
		}
		res.redirect('/');
	});
}


/**
 * POST /user/:id/group
 * add user to group
 */
exports.add_to_group = function(req, res){
	var user_id = req.params.id;
	var group_name = req.body.name;

	db.User.findById(user_id, function(error, user){
		if(error) {
			console.error(error);
		}

		if(user.groups.indexOf(group_name) == -1){
			user.groups.push(group_name);
		
			user.save(function(error, user){
				if(error){
					console.error(error);
				}

				res.redirect('/user/'+user_id);
			});
		} else {
			res.redirect('/user/'+user_id);
		}

	});
}


/**
 * POST /user/:id/group/delete
 * add user to group
 */
exports.remove_from_group = function(req, res){
	var user_id = req.params.id;
	var group_name = req.body.name;

	db.User.findById(user_id, function(error, user){
		if(error) {
			console.error(error);
		}

		user.groups.pull(group_name);
		
		user.save(function(error, user){
			if(error){
				console.error(error);
			}

			res.redirect('/user/'+user_id);
		});

	});


}



