
// Tests run using Mocha (npm install -g mocha)

var db = require('../lib/database');
var should = require('should');
var request = require('supertest');
var app = require('../app');
var async = require('async');


describe('App', function(){

	var test_users = [];
	var numer_of_test_users = 5;

	describe('User creation', function(){

		it('should create a new user', function(done){
			var name = 'test user 0';

			request(app)
				.post('/user')
				.send({name: name})
				.end(function(error, res){

					db.User.findOne({name: name}, function(error, user) {
						
						should.exist(user);
						
						//Store the ID, will need it later
						test_users.push(user.id);

						return done()
					});

				});
		});


		// Create the rest of the test users
		after(function(){
			async.times(numer_of_test_users-1, function(n, next){
				var user = new db.User({name: 'Test user ' + (parseInt(n)+1)  });
				user.save(function(error, user){
					test_users.push(user.id);
					
				});				
			});
		});
		
	});


	describe('Messages', function(){
		var message_id = '';

		it('should send a message from user 0 to user 1', function(done){
			request(app)
				.post('/user/'+test_users[0]+'/message')
				.send({
					body: 'This is a test Message!',
					from: 'Test User 0',
					from_id: test_users[0],
					to: test_users[1]
				})
				.end(function(error, res){
					db.User.findById(test_users[1], function(error, user){
						
						user.messages.should.not.be.empty;

						message_id = user.messages[0]._id;
						return done();
					});
				});
		});


		it('should mark message as viewed', function(done){
			request(app)
				.get('/user/'+test_users[1]+'/message/'+message_id)
				.expect(200)
				.end(function(error, res){
					db.User.findById(test_users[1], function(error, user){

						user.messages.id(message_id).unread.should.be.false;

						return done();
					})
				});
		});


		it('should delete message', function(done){
			request(app)
				.post('/user/'+test_users[1]+'/message/'+message_id+'/delete')
				.expect(200)
				.end(function(error, res){
					db.User.findById(test_users[1], function(error, user){

						should.not.exist(user.messages.id(message_id));

						return done();
					});
				});
		});

	});


	describe('Groups', function(){
		var group_name = "Test Group1!";

		it('should broadcast a message to everyone but user 0', function(done){
			request(app)
				.post('/user/'+test_users[0]+'/message')
				.send({
					body: 'This is a test Message broadcasted to everyone!',
					from: 'Test User 0',
					from_id: test_users[0],
					group: 'all'
				})
				.end(function(error, res){
					async.each(test_users.slice(1), function(user_id, callback){
						db.User.findById(user_id, function(error, user){
							user.messages.should.not.be.empty;

							user.messages = [];
							user.save(function(error){
								return callback(error);
							})

						});
						
					}, function(error){
						return done();
					});
				});
		});


		it('should add user 0 to group '+group_name, function(done){
			request(app)
				.post('/user/'+test_users[0]+'/group')
				.send({name: group_name})
				.expect(200)
				.end(function(error, res){
					db.User.findById(test_users[0], function(error, user){

						user.groups.indexOf(group_name).should.not.equal(-1);

						return done();

					});

				});
		});


		// Add some other users to the same group
		it('should add user 1 to group '+group_name, function(done){
			request(app)
				.post('/user/'+test_users[1]+'/group')
				.send({name: group_name})
				.end(function(error, res){
					db.User.findById(test_users[0], function(error, user){

						user.groups.indexOf(group_name).should.not.equal(-1);

						return done();

					});

				});
		});


		it('should send a message to everyone in the group '+group_name, function(done){
			request(app)
				.post('/user/'+test_users[2]+'/message')
				.send({
					body: 'This is a test Message!',
					from: 'Test User 2',
					from_id: test_users[2],
					group: group_name
				})
				.end(function(error, res){
					async.each([test_users[0], test_users[1]], function(user_id, callback){
						db.User.findById(user_id, function(error, user){
							user.messages.should.not.be.empty;

							return callback();
						});
						
					}, function(error){
						// Make sure user 2 didn't receive message
						db.User.findById(test_users[2], function(error, user){
							if(error || !user){
								console.error(error);
							}
							user.messages.should.be.empty;
							
							return done();
						})
					});
				});
		});


		it('should remove user 1 from group '+group_name, function(done){
			request(app)
				.post('/user/'+test_users[1]+'/group/delete')
				.send({name: group_name})
				.end(function(error, res){
					db.User.findById(test_users[1], function(error, user){
						user.groups.indexOf(group_name).should.equal(-1);

						return done();

					});

				});
		});


	});


	describe('User deletion', function(){
		it('should delete user 0', function(done){
			request(app)
				.post('/user/'+test_users[0]+'/delete')
				.end(function(error, res){

					db.User.findById(test_users[0], function(error, user) {

						should.not.exist(user);
						
						test_users.splice(0,1);
						return done();
					});
				});
		});


		// Clean uo other test users
		after(function(done){
			async.each(test_users, function(user_id, done){
				db.User.findByIdAndRemove(user_id, done);
			}, done);
		});

	});

});


